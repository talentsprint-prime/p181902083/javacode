package assesement1;

import java.util.Scanner;

public class Game {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter puzzle");
		String puzzle = sc.nextLine();
		System.out.println("Enter moves");
		String moves = sc.nextLine();
		System.out.println(inputPuzzle(puzzle));

		doMove(moves, puzzle);

	}

	public static String inputPuzzle(String str) {

		for (int i = 0; i < str.length(); i += 5) {

			System.out.println(str.substring(i, i + 5));

		}
		return str;
	}

	public static String swapSpace(char c, String str) {

		StringBuilder sb = new StringBuilder(str);
		int x = str.indexOf('_');

		if (c == 'A') {

			sb.setCharAt(x, str.charAt(x - 5));
			sb.setCharAt(x - 5, str.charAt(x));

		} else if (c == 'B') {

			sb.setCharAt(x, str.charAt(x + 5));
			sb.setCharAt(x + 5, str.charAt(x));

		} else if (c == 'L') {

			sb.setCharAt(x, str.charAt(x - 1));
			sb.setCharAt(x - 1, str.charAt(x));

		} else if (c == 'R') {

			sb.setCharAt(x, str.charAt(x + 1));
			sb.setCharAt(x + 1, str.charAt(x));

		}

		return sb.toString();
	}

	public static String doMove(String moves, String puzzle) {

		char move = 0;
		int i;

		for (i = 0; i < moves.length(); i++) {

			char c = moves.charAt(i);

			if (puzzle.equals("_ABCDEFGHIJKLMNOPQRSTUVWX") || puzzle.equals("ABCDEFGHIJKLMNOPQRSTUVWX_")) {

				return "End Game";

			} else {

				System.out.println(c);

				if (isValidMove(c, puzzle)) {

					String outputString = (swapSpace(c, puzzle));
					inputPuzzle(outputString);
					puzzle = swapSpace(c, puzzle);
				}
			}
		}
		return inputPuzzle(puzzle);
	}

	public static boolean isValidMove(char c, String puzzle) {

		switch (c) {
		case 'A':
			if (puzzle.indexOf('_') < 5)
				return false;
			else {
				swapSpace(c, puzzle);
				return true;
			}
		case 'B':
			if (puzzle.indexOf('_') > 19)
				return false;
			else {
				swapSpace(c, puzzle);
				return true;
			}
		case 'L':
			if ((puzzle.indexOf('_') % 5) == 0)
				return false;
			else {
				swapSpace(c, puzzle);
				return true;
			}
		case 'R':
			if ((puzzle.indexOf('_') % 5) == 4)
				return false;
			else {
				swapSpace(c, puzzle);
				return true;
			}

		default:
			System.out.println("invalid");
		}
		return true;

	}
}
