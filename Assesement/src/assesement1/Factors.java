package assesement1;

import java.util.Scanner;

public class Factors {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter a number: ");
		num=sc.nextInt();
		
		System.out.println("product of the factors: " + factorsOfGivenNumber(num));

	}
	public static int factorsOfGivenNumber(int num){
		
		int product=1;
		for(int i=1;i<=num;i++)
			if(num%i==0)
				product = product * i;
		return product;
	}

}
