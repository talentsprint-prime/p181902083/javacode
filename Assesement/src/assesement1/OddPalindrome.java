package assesement1;

import java.util.Scanner;

public class OddPalindrome {

	public static void main(String[] args) {

		int start, limit;
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number: ");
		start = sc.nextInt();
		limit = sc.nextInt();

		System.out.println(oddPalindrome(start, limit));

	}

	public static boolean isPalindrome(int num) {
		return (num == reverse(num));
	}

	public static int reverse(int num) {
		int rev = 0;
		int r;
		while (num > 0) {
			r = num % 10;
			rev = rev * 10 + r;
			num = num / 10;
		}
		return rev;
	}

	public static boolean allDigitsOdd(int num) {
			while (num > 0) {
				int r = num % 2;
				if (r % 2 == 0) {
					return false;
				}
				num = num / 10;
			}
		
		return true;
	}

	public static String oddPalindrome(int start, int limit) {

		String result = "";
		for (int i = start; i < limit; i++) {
			if(isPalindrome(i)){
			if (allDigitsOdd(i)) {
				result = result + i+ "\n";
			}
		}}
		return result;
	}
}
