package assesement1;

public class Puzzle1 {
	static int row = 0;
	static int col = 0;

	public static void main(String[] args) {
		char[][] puz = { { 'a', 'b', 'c', 'd', 'e' }, { 'f', 'g', 'h', 'i', 'j' }, { 'k', '-', 'l', 'm', 'n' },
				{ 'o', 'p', 'q', 'r', 's' }, { 't', 'u', 'v', 'w', 'x' } };
		
		
		row = Character.getNumericValue(findEmpty(puz).charAt(0)); 
		col = Character.getNumericValue(findEmpty(puz).charAt(1));
		
		/*System.out.println(doMoves(puz, "ALR"));
		print(puz);*/
		System.out.println(print(puz));

		if (isAlpahabeticOrder(puz))
			System.out.println("END GAME");
		if (row == -1 && col == -1)
			System.out.println("No Configuration");

	}

	public static String findEmpty(char[][] puz) {
		String str = "";
		int i = 0;
		int j = 0;
		for (i = 0; i < 5; i++) {
			for (j = 0; j < 5; j++) {
				if (puz[i][j] == '-') {
					str += i+""+j;
				}
			}
			

		}
		return str;

	}

	public static char[][] swapEmpty(char[][] puz, int row2, int col2) {
		char tmp = puz[row][col];
		puz[row][col] = puz[row2][col2];
		puz[row2][col2] = tmp;
return puz;
	}

	public static boolean isValidMove(char[][] puz, char c) {
		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			swapEmpty(puz, row - 1, col);
			row--;
			
			return true;
		case 'B':
			if (row == 4)
				return false;
			swapEmpty(puz, row + 1, col);
			row++;
			return true;
		case 'L':
			if (col == 0)
				return false;
			swapEmpty(puz, row, col - 1);
			col--;
			return true;
		case 'R':
			if (col == 4)
				return false;
			swapEmpty(puz, row, col + 1);
			col++;
			return true;
		default:
			System.out.println("Bad move: '" + c + "'");
			return false;
		}
	}

	public static char[][] print(char[][] puz) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print((puz[i][j]));
			}
			System.out.println();
		}
		return puz;
	}

	public static void doMoves(char[][] puz, String str) {
		for (int i = 0; i < str.length(); i++) {
			 
				char c = str.charAt(i);
				/*print(puz);
				System.out.println();*/
				System.out.println(c);
				findEmpty(puz);
				if (!isValidMove(puz, c)) {
					row = -1;
					col = -1;
				
				
			}
		}
		
	}

	public static boolean isAlpahabeticOrder(char[][] puz) {
		String str1 = "";
		String str2 = "-abcdefghijklmnopqrstuvwx";
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				str1 += puz[i][j];
			}
		}

		if (str1.equals(str2))
			return true;
		else
			return false;
	}
}
