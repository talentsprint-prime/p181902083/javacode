package assesement1;

public class MaxSpan {

	public static void main(String[] args) {
		int[] arr = { 1,1,1,1,1,1, 2, 2, 3,3,3,3 };
		System.out.println(findMaxFrequency(arr));
	}
	public static int findMaxFrequency(int[] arr) {
		
		java.util.Arrays.sort(arr); 
        
        int max_count = 1, res = arr[0]; 
        int curr_count = 1;
       
        
          
        for (int i = 1; i < arr.length; i++) 
        { 
        	if(arr[i] == 0 || arr[i] < 0){
        		return -1;
        	}
        	
        	
        	
            if (arr[i] == arr[i - 1]) 
                curr_count++; 
            else 
            { 
                if (curr_count > max_count) 
                { 
                    max_count = curr_count; 
                    res = arr[i - 1]; 
                } 
                curr_count = 1; 
            } 
        } 
      
        
        if (curr_count > max_count) 
        { 
            max_count = curr_count; 
            res = arr[arr.length - 1]; 
        } 
      
        return res; 
	}
	
}


