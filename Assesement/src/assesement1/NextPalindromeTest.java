package assesement1;

import static org.junit.Assert.*;

import org.junit.Test;

public class NextPalindromeTest {
	NextPalindrome p = new NextPalindrome();

	@Test
	public void testIsPalindrome() {
		assertEquals(true, p.isPalindrome(121));
	}

	@Test
	public void testReverse() {
		assertEquals(123,p.reverse(321));
	}

	@Test
	public void testNextPalindrome() {
		assertEquals(131,p.nextPalindrome(122));
	}

}
