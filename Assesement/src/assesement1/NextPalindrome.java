package assesement1;

import java.util.Scanner;

public class NextPalindrome {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number: ");
		num = sc.nextInt();

		System.out.println(nextPalindrome(num + 1));

	}

	public static boolean isPalindrome(int num) {
		return (num == reverse(num));
	}

	public static int reverse(int num) {
		int rev = 0;
		int r;
		while (num > 0) {
			r = num % 10;
			rev = rev * 10 + r;
			num = num / 10;
		}
		return rev;
	}

	public static int nextPalindrome(int num) {

		while (!isPalindrome(num)) {
			num++;
		}
		
		return num;
	}

}
