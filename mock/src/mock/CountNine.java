package mock;

public class CountNine {

	public static void main(String[] args) {

		System.out.println(countAllNines(79, 99));
		System.out.println(countAllNines(900, 898));
		System.out.println(countAllNines(9002223, 89232));
		System.out.println(countAllNines(-2, -4));
		System.out.println(countAllNines(10, 15));

	}

	public static int countAllNines(int a, int b) {
		int ctr = 0;
		int j;
		if (a < 0 && b < 0)
			return -1;

		if (a == 0 && b == 0)
			return -2;

		if (a > b) {

			for (j = a; j >= b; j--) {
				if ((j % 10) == 9 || j / 10 == 9)
					ctr++;
			}
			j = j / 10;

		}
		if (b > a) {
			for (j = b; j >= a ; j--) {
				if ((j % 10) == 9 || j / 10 == 9)
					ctr++;
			}
			j = j / 10;

		} 
		if(ctr == 0)
			return -3;
		
		return ctr;
	}
}
