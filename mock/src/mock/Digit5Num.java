package mock;

import java.util.Scanner;

public class Digit5Num {

	public static void main(String[] args) {
	
		int num;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a num:");
		num=sc.nextInt();
		if(num>9999 && num<100000)
		{
			System.out.println(num + " is a five digit");
		}
		else
			System.out.println(num + " is not a five digit");

	}

}
