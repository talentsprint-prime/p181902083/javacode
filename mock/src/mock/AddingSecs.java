package mock;

import java.util.Scanner;

public class AddingSecs {

	public static void main(String[] args) {

		int hrs, min, sec, sec1;
		Scanner sc = new Scanner(System.in);

		System.out.println("Eneter hrs:");
		hrs = sc.nextInt();

		System.out.println("Eneter mins:");
		min = sc.nextInt();

		System.out.println("Enter sec:");
		sec = sc.nextInt();

		System.out.println("Enter the sec to add:");
		sec1 = sc.nextInt();

		int time = (hrs * 60 * 60) + (min * 60) + sec;
		int totaltime = time + sec1;
		System.out.println("Total time : " + calculateHrs(totaltime) + " hours " + calculateMin(totaltime) + " minutes "
				+ calculateSec(totaltime) + " seconds ");
	}

	public static int calculateHrs(int totaltime) {

		int h = totaltime / 3600;
		return h;

	}

	public static int calculateMin(int totaltime) {

		int m = (totaltime % 3600) / 60;
		return m;

	}

	public static int calculateSec(int totaltime) {

		int s = (totaltime % 3600) % 60;
		return s;

	}
}
