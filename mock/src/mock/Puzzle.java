package mock;

import java.util.Scanner;

public class Puzzle {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String puzzle = "ABCDEFGHIJKLMO PQRNTUVWSX";
		String afterMove = puzzle;
		char move;
		String possible;
		int count = 0;
		System.out.println("Welcome to the alphabet puzzle");
		for (int i = 0; i < afterMove.length(); i++) {
			if (i % 5 == 0) {
				System.out.println();
			}
			System.out.print(afterMove.charAt(i) + " ");
		}
		while (isGameOver(afterMove) == false) {
			System.out.println("\nEnter the alphabet to be moved: ");
			possible = possibleMoves(afterMove);
			System.out.println("Possible moves are:\n" + possible);
			move = sc.next().charAt(0);
			if (isValid(move, possible)) {
				afterMove = getBoard(move, afterMove);
				for (int i = 0; i < afterMove.length(); i++) {
					if (i % 5 == 0) {
						System.out.println();
					}
					System.out.print(afterMove.charAt(i) + " ");
				}
				count++;
				System.out.println("\nNumber of moves till now = " + count);
			} else {
				System.out.println("Invalid move! Please try again.");
				for (int i = 0; i < afterMove.length(); i++) {
					if (i % 5 == 0) {
						System.out.println();
					}
					System.out.print(afterMove.charAt(i) + " ");
				}
			}
		}
		System.out.println("\nCongratulations! You have won the game in " + count + " moves.");
	}

	private static boolean isValid(char move, String possible) {
		for (int i = 0; i < possible.length(); i++) {
			if (move == possible.charAt(i) && move != '#') {
				return true;
			}
		}
		return false;
	}

	private static String possibleMoves(String puzzle) {
		String s = "";
		char up = '#', down = '#', left = '#', right = '#';
		for (int i = 0; i < puzzle.length(); i++) {
			if (puzzle.charAt(i) == ' ') {
				if (i > 4) {
					up = puzzle.charAt(i - 5);
				}
				if (i < 21) {
					down = puzzle.charAt(i + 5);
				}
				if (i > 0 && i % 5 != 0) {
					left = puzzle.charAt(i - 1);
				}
				if (i < 25 && (i - 4) % 5 != 0) {
					right = puzzle.charAt(i + 1);
				}
			}
		}
		s += up + "  " + down + "  " + left + "  " + right;
		return s;
	}

	public static String getBoard(char move, String afterMove) {
		String newPuzzle = "";
		int temp = 0, temp1 = 0;
		for (int i = 0; i < afterMove.length(); i++) {
			if (afterMove.charAt(i) == ' ') {
				temp = i;
				break;
			}
		}
		for (int i = 0; i < afterMove.length(); i++) {
			if (i != temp && i != temp1) {
				newPuzzle += afterMove.charAt(i);
			}
			if (i == temp) {
				newPuzzle += afterMove.charAt(temp1);
			}
			if (i == temp1) {
				newPuzzle += afterMove.charAt(temp);
			}
		}
		return newPuzzle;
	}

	public static boolean isGameOver(String puzzle) {
		String win = "ABCDEFGHIJKLMNOPQRSTUVWX";
		if (win.equals(puzzle.trim())) {
			return true;
		} else {
			return false;
		}
	}
}
