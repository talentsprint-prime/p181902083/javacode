package mock;

public class MakeBricks {

	public static void main(String[] args) {

		System.out.println("isWallPossible(3,1,8) Required true => Actual " + isWallPossible(3, 1, 8));
		System.out.println("isWallPossible(3,1,9) Required false => Actual " + isWallPossible(3, 1, 9));
		System.out.println("isWallPossible(40,2,50) Required true => Actual " + isWallPossible(40, 2, 50));
		System.out.println("isWallPossible(40,2,52) Required false => Actual " + isWallPossible(40, 2, 52));
	}

	public static boolean isWallPossible(int noSmall, int noBig, int goal) {
		if (goal == 0)
			return true;
		

		if (goal < noBig * 5) {
			goal = goal - noSmall;
			while (goal > 0) {
				goal = goal - 5;
				--noBig;
				if (noBig == 0) {
					break;
				}

			}
			return (goal == 0);
		} else {
			goal = goal - (noBig * 5); 
			while (goal > 0) {
				goal = goal - 1;
				--noSmall;
				if (noSmall == 0) {
					break;
				}
				
			}
			return (goal == 0);
		}
		
	}

}
