package mock;

public class SixthStarPattern {
	public static void main(String[] args) {
		System.out.println(getStars(10));
	}

	public static String getStars(int n) {
		if (n <= 0)
			return "error";

		for (int row = 1; row <= n; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print("*");

			}
			System.out.println();
		}

		for (int row = n - 1; row >= 1; row--) {

			for (int col = 1; col <= row; col++) {
				System.out.print("*");

			}
			System.out.println();

		}
		return "";
	}

}
