package loops;

import java.util.Scanner;

public class SecondMax {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number: ");
		num = sc.nextInt();
		System.out.println("SecondMax digit of a given number: " + calculatingSecondMaxDigit(num));
		
	}

	public static int calculatingSecondMaxDigit(int n) {
		int max = 0,r;
		int secondmax = 0;
		while (n > 0) {
			 r = n % 10;
			if (max < r) {
				secondmax = max;
				max = r;
			} else if (secondmax < r && r != max) {
				secondmax = r;
			}
			n = n / 10;
		}
		return secondmax;
	
	}

}
