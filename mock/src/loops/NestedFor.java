package loops;

public class NestedFor {

	public static void main(String[] args) {
		int stars=5;
		for(int row=1;row<=stars;row++){
			for(int col =1;col<=row;col++){
				System.out.print(row+" ");
			}
			System.out.print("\n");
		}
		
	}

}
