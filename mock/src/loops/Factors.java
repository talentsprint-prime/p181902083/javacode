package loops;

import java.util.Scanner;

public class Factors {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Eneter a num:");
		num = sc.nextInt();
		System.out.println("Factors of " + num + " is:" + calculateFactors(num));
	}

	public static String calculateFactors(int n) {
		int i = 1;
		String str = "";
		while (i <= n) {
			if (n % i == 0)
			{
				str = str + i + " ";
			}
			i++;
		}
		return str;

	}
}
