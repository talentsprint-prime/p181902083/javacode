package loops;

import java.util.Scanner;

public class NaturalNumbers {

	public static void main(String[] args) {
		int i = 1,n; 
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter n value: ");
		n=sc.nextInt();
		while (i <= n) {
			System.out.println(i);
			i++;

		}
		System.out.println("final value: " + i);

	}

}
