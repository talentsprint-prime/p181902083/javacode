package loops;

import java.util.Scanner;

public class Numbers {

	public static void main(String[] args) {
		int num;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a num: ");
		num = sc.nextInt();

		System.out.println("Factorial of a given number: " + factorialOfNumber(num));

	}

	public static int factorialOfNumber(int n) {
		int fact = 1;
		for (int i = 1; i <= n; i++) {
			fact = fact * i;
		}
		return fact;
	}

}
