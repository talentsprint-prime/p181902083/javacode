package loops;

import java.util.Scanner;

public class DigitsOfNumber {

	public static void main(String[] args) {
		int num, digit, rev = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter num value: ");
		num = sc.nextInt();
		System.out.println("Number Before Reverse: " + num);
		while (num > 0) {
			digit = num % 10;
			rev = rev * 10 + digit;
			num = num / 10;
		}
		System.out.println("Number After Reverse: " + rev);
	}
}
