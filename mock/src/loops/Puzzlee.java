package loops;

public class Puzzlee {
	static int row;
	static int col;

	public static void main(String[] args) {
		char[][] puz = { { 'a', 'b', 'c', 'd', 'e' }, { 'f', '-', 'h', 'i', 'j' }, { 'k', 'd', 'l', 'm', 'n' },
				{ 'o', 'p', 'q', 'r', 's' }, { 't', 'u', 'v', 'w', 'x' } };
		/*
		 * System.out.println(findEmpty(puz));
		 * System.out.println(isValid(puz,'A'));
		 */
		System.out.println(doMove(puz,"A"));
	}

	public static String findEmpty(char[][] puz) {
		String str = "";
		int i = 0;
		int j = 0;
		for (i = 0; i < 5; i++) {
			for (j = 0; j < 5; j++) {
				if (puz[i][j] == '-') {
					str += i + "" + j;
				}
			}

		}
		return str;

	}

	public static boolean isValid(char[][] puz, char c) {
		int row = Character.getNumericValue(findEmpty(puz).charAt(0));
		int col = Character.getNumericValue(findEmpty(puz).charAt(1));

		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			else
				return true;
		case 'B':
			if (row == 4)
				return false;
			else
				return true;
		case 'L':
			if (col == 0)
				return false;
			else
				return true;
		case 'R':
			if (col == 4)
				return false;
			else
				return true;
		default:
			System.out.println("Bad move: '" + c + "'");
			return false;
		}
	}

	public static void swapEmpty(char[][] puz, int row1, int col1) {
		char tmp = puz[row][col];
		puz[row][col] = puz[row1][col1];
		puz[row1][col1] = tmp;
	}

	public static char[][] doMove(char[][] puz, String moves) {
		for (int i = 0; i < moves.length(); i++) {
			char ch = moves.charAt(i);
			System.out.println(ch);
			System.out.println();
			if (isValid(puz, ch)) {
				switch (ch) {
				case 'A':
					swapEmpty(puz, row - 1, col);
					row--;
					print(puz);
				}
			}
		}
		return puz;
	}

	public static char[][] print(char[][] puz) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.println(puz[i][j]);
			}
			System.out.println();
		}
		return puz;
	}
}