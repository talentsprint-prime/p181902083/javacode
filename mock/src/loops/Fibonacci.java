package loops;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number: ");
		num = sc.nextInt();
		System.out.println("Fiboncci series: " + calculatingFibonacciSeries(num));
	}

	public static String calculatingFibonacciSeries(int n) {
		int f1 = 0, f2 = 1, f3 = 0;
		String str = "";
		for (int i = 0; i < n; i++) {
			str = str + f1 + " ";
			f3 = f1 + f2;
			f1 = f2;
			f2 = f3;
			}
		return str;

	}
}
