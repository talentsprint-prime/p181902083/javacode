package loops;

public class ReverseOfStars {

	public static void main(String[] args) {
		int stars=4;
		for(int row=1;row<=stars;row++){
			for(int col =stars;col>=row;col--){
				System.out.print("* ");
			}
			System.out.print("\n");

	}

}
}
