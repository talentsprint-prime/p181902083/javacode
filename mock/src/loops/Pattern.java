package loops;

public class Pattern {

	public static void main(String[] args) {
		int stars=5;
		for(int row=1;row<=stars;row++){
			for(int col=stars;col>=row;col--){
				System.out.print(" ");
				}
			for(int col=1;col<=row;col++)
			{
				System.out.print("* ");
			}
			System.out.println();

	}

}
}
