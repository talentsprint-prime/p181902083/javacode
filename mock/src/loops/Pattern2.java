package loops;

public class Pattern2 {

	public static void main(String[] args) {
		
		int stars = 5;
		for (int row = stars; row >= 1; row--) {

			for (int col = 1; col <= row; col++) {
				System.out.print("* ");

			}
			System.out.println();
		}
	}

}

