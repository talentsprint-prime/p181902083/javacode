package loops;

import java.util.Scanner;

public class EvenOddDigits {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter number: ");
		num = sc.nextInt();
		System.out.println(countEvenOdd( num));
	}
	public static String countEvenOdd(int n)
	{
		
		int Evencount = 0;
		int Oddcount = 0;
		int r = 0;
		
		while (n> 0) {
			r = n % 10;
			n = n / 10;
			if (r % 2 == 0) {
				Evencount++;

			} else {
				Oddcount++;

			}

		}
		return "Evendigits:" + Evencount + " "+ " Odddigits:" + Oddcount;
	}
}
