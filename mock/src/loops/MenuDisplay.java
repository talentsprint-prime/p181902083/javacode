package loops;

import java.util.Scanner;

public class MenuDisplay {

	public static void main(String[] args) {
		int num;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a value: ");
		int num1 = sc.nextInt();
		
		System.out.println("Enter b value: ");
		int num2 = sc.nextInt();

		while (true) {
			
			int add, sub, mul, div, power, rem;
			System.out.println("\n1.Add\n2.sub\n3.mul\n4.div\n5.rem\n6.power\n7.exit");
			
			System.out.println("enter a number to perform a ation: ");
			num = sc.nextInt();
			
			switch (num) {
			case 1:
				add = num1 + num2;
				System.out.println("sum: "+add);
				break;
			case 2:
				sub = num1 - num2;
				System.out.println("sub: "+sub);
				break;
			case 3:
				mul = num1 * num2;
				System.out.println("mul: "+mul);
				break;
			case 4:
				div = num1 / num2;
				System.out.println("div: "+div);
				break;
			case 5:
				rem = num1 % num2;
				System.out.println("rem: "+rem);
				break;
			case 6:
				System.out.println("power: " + Math.pow(num1, num2));
				break;
			case 7:
				System.exit(0);
				break;

			default:
				System.out.println("invalid");

			}
		}

	}

}
