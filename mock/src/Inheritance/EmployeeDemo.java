package Inheritance;

public class EmployeeDemo {

	public static void main(String[] args) {
		Programmer p1 = new Programmer("Laxmi",25000,"Gachibowli",9999999999l,"java");
		System.out.println(p1);
		p1.work();
		
		System.out.println("programmer :"+p1.getId());
		
		Employee e1 = new Employee("abc",10000,"Dlf",88888888l);
		System.out.println(e1);
		e1.work();
		
		Employee e2 =new Programmer("xyz",15000,"abc",777777l,"c");
		System.out.println(e2);
		e2.work();
		
		System.out.println("programmer :"+e2.getId());
		

	}

}
