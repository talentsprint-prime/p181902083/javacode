package Inheritance;

public class Student extends Person {
	private String programme;
	private int year;
	private double fee;

	public Student() {
		super();
	}

	public Student(String name, String address, String programme, int year, double fee) {
		super(name, address);
		this.programme = programme;
		this.year = year;
		this.fee = fee;
	}

	public String getProgramme() {
		return programme;
	}

	public void setProgramme(String programme) {
		this.programme = programme;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	@Override
	public String toString() {
		return "Student [programme=" + programme + ", year=" + year + ", fee=" + fee + ", toString()="
				+ super.toString() + "]";
	}
	

	
}


