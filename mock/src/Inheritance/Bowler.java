package Inheritance;

public  class Bowler extends PlayerCricket {
	
	private int wickets;
	private int runsGiven;
	private int ballsBowled;
	
	public double calcAverage(){
		return (double)runsGiven / wickets;
	}
	public double calEconomy(){
		return (double)runsGiven /(ballsBowled * 6);
	}
	
	
	



	public Bowler(String name, int matches, int wickets, int runsGiven, int ballsBowled) {
		super(name, matches);
		this.wickets = wickets;
		this.runsGiven = runsGiven;
		this.ballsBowled = ballsBowled;
	}
	@Override
	public String toString() {
		return "Bowler [wickets=" + wickets + ", runsGiven=" + runsGiven + ", ballsBowled=" + ballsBowled
				+ ", toString()=" + super.toString() + "]";
	}



	public int getWickets() {
		return wickets;
	}



	public void setWickets(int wickets) {
		this.wickets = wickets;
	}



	public int getRunsGiven() {
		return runsGiven;
	}



	public void setRunsGiven(int runsGiven) {
		this.runsGiven = runsGiven;
	}



	public int getBallsBowled() {
		return ballsBowled;
	}



	public void setBallsBowled(int ballsBowled) {
		this.ballsBowled = ballsBowled;
	}
	
	
	
	
	
	
	
	
	

	
	

}
