package Inheritance;

public abstract class PlayerCricket {
	private int id;
	private String name;
	private int matches;
	private static int  idGenerator = 100;
	
	public abstract double  calcAverage();
		
	
	public PlayerCricket(){
		super();
		
	}
	public PlayerCricket( String name, int matches) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.matches = matches;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", matches=" + matches + "]";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMatches() {
		return matches;
	}
	public void setMatches(int matches) {
		this.matches = matches;
	}
	public static int getIdGenerator() {
		return idGenerator;
	}
}
