package Inheritance;

public class Employee {

	private int id;
	private String name;
	private double salary;
	private String address;
	private long phonenum;
	
	private static int idGenerator = 100;
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhonenum() {
		return phonenum;
	}

	public void setPhonenum(long phonenum) {
		this.phonenum = phonenum;
	}

	public static int getIdGenerator() {
		return idGenerator;
	}

	public Employee(){
		super();
		this.id = ++idGenerator;
	}
	
	//parameterized constructor
	public Employee(String name, double salary, String address, long phonenum) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.salary = salary;
		this.address = address;
		this.phonenum = phonenum;
	}
	
	public void work(){
		System.out.println("I am employee!! doing some work");
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", address=" + address + ", phonenum="
				+ phonenum + "]";
	}


	

}
