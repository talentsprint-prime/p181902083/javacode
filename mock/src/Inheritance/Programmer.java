package Inheritance;

public class Programmer extends Employee {
	
	private String programmingLang;


	public Programmer(){
		super();

	}


	public Programmer(String name, double salary, String address, long phonenum, String programmingLang) {
		super(name, salary, address, phonenum);
		this.programmingLang = programmingLang;
	}


	public String getProgrammingLang() {
		return programmingLang;
	}


	public void setProgrammingLang(String programmingLang) {
		this.programmingLang = programmingLang;
	}


	@Override
	public String toString() {
		return "Programmer [programmingLang=" + programmingLang + ", toString()=" + super.toString() + "]";
	}
	
	public void work(){
		System.out.println("I am a programmer!! write some code");
	}


	
	
}