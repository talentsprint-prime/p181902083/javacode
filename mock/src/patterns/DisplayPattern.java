package patterns;

public class DisplayPattern {

	public static void main(String[] args) {

		int stars = 5;
		for (int row = stars; row >= 1; row--) {

			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");

			}
			System.out.println();
		}
		for (int row = 2; row <= stars; row++) {
			for (int col = 1; col <= row; col++) {
				System.out.print(col + " ");

			}
			System.out.println();
		}

		System.out.println();
	}

}
