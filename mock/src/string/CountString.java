package string;

import java.util.Scanner;

public class CountString {
	public static void main(String args[]) {

		String str;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a string: ");
		str = sc.nextLine();

		char ch;
		int vowels = 0, consonants = 0, digits = 0;
		String str2 = "";
		String str1 = str.toLowerCase();

		for (int i = 0; i < str1.length(); i++) {
			ch = str.charAt(i);

			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				++vowels;
			} else if ((ch >= 'a' && ch <= 'z')) {
				++consonants;
			} else if (ch >= '0' && ch <= '9') {
				++digits;
			}

		}

		for (int i = 0; i < str.length(); i++) {
			char c =str.charAt(i);
			if(c >= 'a' && c <= 'z' || c >= 'A'&& c <='Z')
			{
				c = (char)(c + 1);
				str2 = str2 + c;
			}else
				str = str + c;
		}

		
		System.out.println("Vowels: " + vowels);
		System.out.println("Consonants: " + consonants);
		System.out.println("Digits: " + digits);
		System.out.println(str2);
	}

}
