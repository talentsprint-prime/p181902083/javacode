package string;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a String: ");
		String str = sc.nextLine();
		if (isPalindrome(str) == true) {
			System.out.println(str + " is a palindrome");

		} else
			System.out.println(str + " is not a palindrome");
	}

	public static boolean isPalindrome(String str) {

		for (int i = str.length() - 1, j = 0; i > j; i--, j++)
			if (str.charAt(i) != str.charAt(j)) {
				return false;
			}
		return true;

	}

}
