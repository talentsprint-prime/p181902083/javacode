package string;

import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a string: ");
		str=sc.nextLine();
		String arr[] = str.split(" ");
		
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
		System.out.println("Number of words in a string: " + arr.length);
	}

}
