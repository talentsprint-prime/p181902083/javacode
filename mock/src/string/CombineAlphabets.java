package string;

import java.util.Scanner;

public class CombineAlphabets {

	public static void main(String[] args) {
		String str;
		char ch;
		String Alphabets = "";
		String specialchar = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string:");
		str = sc.nextLine();
		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);
			if (Character.isUpperCase(ch) || Character.isLowerCase(ch)) {
				Alphabets = Alphabets + ch;
			} else if ((int) ch >= 33 && (int) ch <= 47) {
				specialchar = specialchar + ch;
			}
		}

		System.out.println(specialchar + Alphabets);
	}

}
