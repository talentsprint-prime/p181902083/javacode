package string;

public class StringDemo {

	public static void main(String[] args) {
		String str = "india";
		int len = str.length();
		System.out.println("length of a string: "+len);
		System.out.println("char at a position: "+str.charAt(0));
		System.out.println("last index : "+str.lastIndexOf('i'));
		System.out.println("index position: "+str.indexOf('i'));
		

	}

}
