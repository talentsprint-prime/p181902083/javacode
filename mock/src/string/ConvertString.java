package string;

import java.util.Scanner;

public class ConvertString {

	public static void main(String[] args) {
		String str;
		char c;
		int ascii=0;
		String result="";
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a string: ");
		str = sc.nextLine();
	
		for(int i=0;i<str.length();i++)
		{
			c = str.charAt(i);
			if(c >= 'a' && c <= 'z'){
				ascii = (int)c - 32;
			}else if(c >= 'A' && c <= 'Z'){
				ascii = (int)c + 32;
			}else{
				ascii = (int)c;
			}
			result+=(char)ascii;
		}
		System.out.println(result);

	}

}
