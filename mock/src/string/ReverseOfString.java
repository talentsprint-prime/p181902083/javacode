package string;

import java.util.Scanner;

public class ReverseOfString {

	public static void main(String[] args) {
		String str;
		Scanner sc = new Scanner(System.in);
		String str1="";

		System.out.println("Enter a string: ");
		str = sc.nextLine();
		String rev = "";
		String arr[] = str.split(" ");
		for (int i = 0; i < arr.length; i++) {
			String result = arr[i];

			for (int j = result.length() - 1; j >= 0; j--) {
				rev = rev + result.charAt(j);
			}
			str1 = str1+rev + " ";
			rev="";
		}

		System.out.println(str1);
		

	}
}
