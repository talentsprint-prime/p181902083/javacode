package string;

import java.util.Scanner;

public class StringDisplay {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a string: ");
		String name=sc.nextLine();
		
		int length = name.length();
		System.out.println(length);
		
		int index = name.indexOf(" ");
		
		System.out.println("first name: "+name.substring(0,index));
		System.out.println("last name: "+name.substring(index, length));

	}

}
