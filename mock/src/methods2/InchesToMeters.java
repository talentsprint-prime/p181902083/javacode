package methods2;

import java.util.Scanner;

public class InchesToMeters {

	public static void main(String[] args) {
		int inches;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter inches: ");
		inches = sc.nextInt();
		System.out.println(convertIntoMeters(inches));

	}

	public static double convertIntoMeters(int inches) {
		double meters = inches * 0.0254;
		return meters;
	}

}
