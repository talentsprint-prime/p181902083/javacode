package methods2;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {

		int num1, num2, num3;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter num1 value: ");
		num1 = sc.nextInt();
		System.out.println("Enter num2 value: ");
		num2 = sc.nextInt();
		System.out.println("Enter num3 value: ");
		num3 = sc.nextInt();
		System.out.println(calculateAvg(num1, num2, num3));

	}

	public static double calculateAvg(int num1, int num2, int num3) {
		double average = (calculateSum(num1, num2, num3)) / 3.0;
		return average;
	}

	public static int calculateSum(int num1, int num2, int num3) {
		int sum = num1 + num2 + num3;
		return sum;
	}
}