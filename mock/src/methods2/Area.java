package methods2;

import java.util.Scanner;

public class Area {

	public static void main(String[] args) {
		int r;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter radius of a circle: ");
		r = sc.nextInt();
		System.out.println("Area of  circle: " + calculateArea(r));
		System.out.println("perimeter of circle: " + calculatePerimeter(r));

	}

	public static double calculateArea(int r) {
		double area = 3.14 * r * r;
		return area;

	}

	public static double calculatePerimeter(int r) {
		double perimeter = 2 * 3.14 * r;
		return perimeter;
	}

}
