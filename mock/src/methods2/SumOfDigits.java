package methods2;

import java.util.Scanner;

public class SumOfDigits {

	public static void main(String[] args) {

		int num;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter a value:");
		num = sc.nextInt();
		System.out.println("Sum of twodigit number is: " +addingNumbers(num));

	}

	public static int addingNumbers(int num) {
		int sum = (num % 10) + (num / 10);
		return sum;
	}

}
