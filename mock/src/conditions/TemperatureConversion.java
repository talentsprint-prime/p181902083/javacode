package conditions;

import java.util.Scanner;

public class TemperatureConversion {

	public static void main(String[] args) {

		double temp;
		int ch;
		double F, C;

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter temp:");
		temp = sc.nextDouble();
		System.out.println("Enter CH value");
		ch = sc.nextInt();
		switch (ch) {
		case 0:
			F = ((9 * temp) / 5) + 32;
			System.out.println("Value is: " + F + " f ");
			break;
		case 1:
			C = ((temp - 32) * 5) / 9;
			System.out.println("Value is: " + C + " c ");
			break;
		default:
			System.out.println("Invalid");
		}

	}

}
