package conditions;

import java.util.Scanner;

public class GradeCalculation {

	public static void main(String[] args) {

		int sub1, sub2, sub3;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter sub1 marks: ");
		sub1 = sc.nextInt();
		System.out.println("Enter sub2 marks: ");
		sub2 = sc.nextInt();
		System.out.println("Enter sub3 marks: ");
		sub3 = sc.nextInt();

		double percent;
		percent = ((sub1 + sub2 + sub3) / 3);
		System.out.println("Percentage is: " + percent + " % ");
		if (percent >= 90) {
			System.out.println("Grade A");
		} else if (percent >= 70) {
			System.out.println("Grade B");
		} else if (percent >= 50) {
			System.out.println("Grade C");
		} else {
			System.out.println("Fail");
		}

	}

}
