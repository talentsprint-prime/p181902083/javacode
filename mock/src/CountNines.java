
public class CountNines {
	public static int getCountNines(int a, int b) {
		int ctr = 0;
		int j;
		if (a < 0 && b < 0)
			return -1;

		if (a == 0 && b == 0)
			return -2;

		if (a > b) {

			for (j = a; j >= b; j--) {
				if ((j % 10) == 9 || j / 10 == 9)
					ctr++;
			}

		}
		if (b > a) {
			for (j = b; j >= a; j--) {
				if ((j % 10) == 9 || j / 10 == 9)
					ctr++;
			}

		}
		if (ctr == 0)
			return -3;

		return ctr;

	}

	public static void main(String[] args) {
		System.out.println(getCountNines(30, 20));

	}


}
