package modularpackage;

public class PerfectSquare4Digit {

	public static void main(String[] args) {
		
		
		System.out.println(perfectSquares(1000,9999));

	}
	
	public static boolean count4Digits(int num) {
		
		int count = 0;
		
		while(num > 0){
			num = num / 10;
			count ++;
			
			if (count == 4)
				return true;
		}
		return false;
	}
	
	public static boolean allDigitsEven(int num) {
		
		int digit = 0  ;

			while (num > 0)  {
		        digit = num % 10; 
		        if (digit % 2 != 0) {
		            return false;
		            }
		         num = num /10;
		        
		       }
		        
         return true;
	}
	
	public static boolean isPerfect(int num){
		 
		int square;
		 
		square = (int) Math.sqrt(num);
		
		if(num == Math.pow(square, 2)) {
			 return true;
		 }
		 return false;
		 
	}
	
	public static String perfectSquares(int start,int limit) {
		
		String str = "";
		
		for (int i = start; i <= limit ; i++) {
			if(count4Digits(i)) 
				if(allDigitsEven(i)) 
					if (isPerfect(i)) 
						str = str + i + " ";
					}
		return str;
	}
			}
