package modularpackage;

public class LargestPowerOfCollatz {

	public static void main(String[] args) {
		System.out.println(getCollatzSequence(64));
		System.out.println(highestPower(64));
	}

	public static int highestPower(int num) {

		String str = getCollatzSequence(num);

		String[] stringArray = str.split(",");

		int len = stringArray.length;
		int i = len - 2;
		int count = 0;

		if ((int) (Math.ceil((Math.log(num) / Math.log(2)))) == (int) (Math.floor(((Math.log(num) / Math.log(2)))))) {
			
			return (int) (Math.ceil((Math.log(num) / Math.log(2))));
		} 
		else {
			while (Integer.parseInt(stringArray[i]) % 2 == 0) {
				i--;
			}
			int num1 = Integer.parseInt(stringArray[i + 1]);

			while (num1 != 1) {
				count++;
				num1 = num1 / 2;
			}

		}

		return count;

	}

	public static String getCollatzSequence(int number) {
		String str = "";

		while (number != 4) {

			str = str + number + ",";

			if (number % 2 != 0) {
				number = 3 * number + 1;
			}

			else {
				number = number / 2;
			}

		}

		str = str + number + ",2,1";
		return str;
	}

}
