package modularpackage;

public class Amicable {

	public static void main(String[] args) {

		System.out.println(amicablePairs(100, 1000));
	}

	public static String amicablePairs(int num1, int num2) {
		String str = "";
		for (int i = num1; i <= num2; i++) {
			int num = sumOfFacotrs(i);
			if (num > i && num < num2)

				str += i + ":" + num + "\n";

		}
		return str;

	}

	public static int sumOfFacotrs(int num) {
		int sum = 0;

		for (int i = 2; i * i < num; i++) {
			if (num % i == 0)
				sum += i + (num / i);

		}

		return sum + 1;
	}
}
