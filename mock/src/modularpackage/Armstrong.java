package modularpackage;

public class Armstrong {

	public static void main(String[] args) {
		System.out.println(printArmstrongs(10, 1000));

	}

	public static boolean isArmstrong(int num) {
		return (num == checkArmstrong(num));
	}

	public static int checkArmstrong(int num) {
		int r = 0, sum = 0;
		int len = ((int) (Math.log10(num) + 1));

		while (num > 0) {
			r = num % 10;
			sum += Math.pow(r, len);
			num = num / 10;
		}

		return sum;

	}

	public static String printArmstrongs(int start, int limit) {
		String result = "";
		
		if(start <= 0 || limit <= 0)
			return "-1";
		
		if(start >= limit)
			return "-2";
		
		for (int i = start; i <= limit; i++) {

			if (isArmstrong(i))
				result = result + i + ",";
			
		}
		if(result.length() == 0)
			return "-3";
		else
			result = result.substring(0, result.length()-1);
			return result;
	}

}
