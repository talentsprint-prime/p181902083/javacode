package modularpackage;

public class PerfectSquareInRange {

	public static void main(String[] args) {
	
		System.out.println(perfectSquares(1000,6000));

	}

	public static boolean allDigitsEven(int num) {
		int digit = 0;

		while (num > 0) {
			digit = num % 10;
			if (digit % 2 != 0) {
				return false;
			}
			num = num / 10;
		}
		return true;
	}

	

	public static String perfectSquares(int start, int limit) {

		String str = "";
	
        
		for (int i = (int) Math.sqrt(start) + 1; i <= (int) Math.sqrt(limit); i = i + 2) {
			
			int num = i * i;
			if (allDigitsEven(num))
			
				str = str + num + " ";
		}
		return str;
	}

}
