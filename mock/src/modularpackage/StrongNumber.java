package modularpackage;

public class StrongNumber {

	public static void main(String[] args) {
		System.out.println(sumOfFactorials(145));
		System.out.println(isStrongNumber(145));
		
	}
	public static boolean isStrongNumber(int num){
		return (num == sumOfFactorials(num));
	}
	public static int sumOfFactorials(int num){
		int sum = 0;
		while(num > 0){
			sum += fact(num % 10);
			num /= 10;
		}
		
	return sum;
	}
	public static int fact(int num){
		int product = 1;
		
		for(int i = num;i >= 1;i--)
			product = product * i;
		
	
	return product;
	
	
}
}
