package modularpackage;

public class Boolean {

	public static void main(String[] args) {
		System.out.println(booleanMethod(false, true, true));

	}

	public static boolean booleanMethod(boolean a, boolean b, boolean c) {
		int count = 0;
		if (a == true)
			count++;
		if (b == true)
			count++;
		if (c == true)
			count++;

		if (count == 2)
			return true;
		else
			return false;

	}
}