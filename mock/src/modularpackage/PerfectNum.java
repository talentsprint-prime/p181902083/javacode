package modularpackage;

import java.util.Scanner;

public class PerfectNum {

	public static void main(String[] args) {
		int num;
		int sum = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter a num: ");
		num = sc.nextInt();
		
		if (checkPerfectNumber(num) == num)
			System.out.println(num + " is a perfect number");
		else
			System.out.println(num + " is not a perfect number");
	}
	
	public static int checkPerfectNumber(int n)
	{
		int sum=0;
		for (int i = 1; i < n; i++) {
			if (n % i == 0) {
				sum += i;
			}
		}
		return sum;
	}

}
