package modularpackage;

import java.util.Scanner;

public class CountVowels {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("enter input");
		String str = sc.nextLine();
		System.out.println("number of vowels:" + countVowels(str) + "\n");
	}

	public static int countVowels(String str) {
		int count = 0;

		for (int i = 0; i < str.length(); i++) {

			char ch = str.charAt(i);
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				count++;
			}

		}
		return count;
	}

}
