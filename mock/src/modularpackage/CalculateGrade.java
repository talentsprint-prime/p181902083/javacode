package modularpackage;

import java.util.Scanner;

public class CalculateGrade {

	private static int sub3;

	public static void main(String[] args) {

		int sub1, sub2, sub3;
		double percent = 0.0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter sub1 marks: ");
		sub1 = sc.nextInt();
		System.out.println("Enter sub2 marks: ");
		sub2 = sc.nextInt();
		System.out.println("Enter sub3 marks: ");
		sub3 = sc.nextInt();
		System.out.println(calculatingPercentage(sub1, sub2, sub3));
		System.out.println(calculatingGrades(sub1, sub2, sub3));

	}

	public static double calculatingPercentage(int sub1, int sub2, int sub) {
		double percent = ((sub1 + sub2 + sub) / 3.0);
		return percent;
	}

	public static char calculatingGrades(int sub1, int sub2, int sub) {

		char ch;

		if (calculatingPercentage(sub1, sub2, sub) >= 90) {
			ch = 'A';
		} else if (calculatingPercentage(sub1, sub2, sub) >= 70) {
			ch = 'B';
		} else if (calculatingPercentage(sub1, sub2, sub) >= 50) {
			ch = 'C';
		} else {
			ch = 'F';

		}
		return ch;
	}

}
