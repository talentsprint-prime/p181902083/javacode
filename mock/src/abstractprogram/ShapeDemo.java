package abstractprogram;

public class ShapeDemo {

	public static void main(String[] args) {
		
		Circle c1 = new Circle("blue",true,5l);
		System.out.println(c1);
		
		Rectangle r1 = new Rectangle("blue",true,15,10);
		System.out.println(r1);

	}

}
