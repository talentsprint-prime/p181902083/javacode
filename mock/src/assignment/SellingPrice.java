package assignment;

import java.util.Scanner;

public class SellingPrice {

	public static void main(String[] args) {
		int cost;
		double SP;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the cost of an item: ");
		cost = sc.nextInt();
		SP = calculateSellingPrice(cost);

		System.out.println("original cost of an item: " + SP);

	}

	public static int calculateSellingPrice(int cost) {
		if (cost <= 10000) {
			return cost - ((cost * 10) / 100);
		} else if ( cost <= 20000) {
			return cost - ((cost * 20) / 100);
		} else
			return cost - ((cost * 25) / 100);
	}

}
