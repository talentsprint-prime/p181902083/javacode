
public class LuckyNumber01 {
	public static void main(String args[]){
		
	String date = "15-APR-2016";
	System.out.println(getLuckyNumber(date));
}

   public static int getLuckyNumber(String date) {
	   String[] dateparts = date.split("-");
	     
	    int dd = Integer.parseInt(dateparts[0]);
	    int mm = convertMMMtoMM(dateparts[1]);
	    int year = Integer.parseInt(dateparts[2]);
	    
	    int sum1 = getSumOfDigits(dd);
	    sum1 += getSumOfDigits(mm) ;
	    sum1 += getSumOfDigits(year);
	    while(sum1 >= 10){
	    	sum1 = getSumOfDigits(sum1);
	    }
	    return sum1;
	    
	}

	public static int convertMMMtoMM(String mon) {
		String Months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();
		int m = ((Months.indexOf(mon) / 3) + 1);
		
		return m;

	}

	public static int getSumOfDigits(int num) {

		int  sum = 0;
		while (num > 0) {
			
			sum += num % 10;
			num = num / 10;

		}
	
		return sum;
	}
}
