
public class FourPerLine01 {
	public static void main(String[] args) {
		int num = 20;
		System.out.println(getFourPerLine(num));
	}

	public static String getFourPerLine(int num) {
		String str = "";
		if(num < 1 || num > 99)
			return "-1";
		
		for (int i = 1; i <= num; i++) {
			if (i % 4 == 0) {
				if (i <= 9) 
					str += " "+ i + "\n";
				else
					str += i + "\n";
				}
			else 
				if (i <= 9) 
					str += " " + i + " ";
				else if(i == num)
					str += i ;
				else
					str += i + " ";
				

		}
		return str;

	}
}
