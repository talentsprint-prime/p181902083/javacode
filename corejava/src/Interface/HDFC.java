package Interface;

public class HDFC implements Bank {
	private static final double rateOfInterset = 0;
	private int id;
	private String branch;
	private float rateOfInterest;
	 Customer cust;
	
	public HDFC() {
		super();
	}

	
	
	public HDFC(int id, String branch, float rateOfInterest, Customer cust) {
		super();
		this.id = id;
		this.branch = branch;
		this.rateOfInterest = rateOfInterest;
		this.cust = cust;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public float getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(float rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}
	

	


	@Override
	public String toString() {
		return "HDFC [id=" + id + ", branch=" + branch + ", rateOfInterest=" + rateOfInterest + ", cust=" + cust
				+ ", calcSI()=" + calcSI() + "]";
	}



	@Override
	public double calcSI() {
		return (cust.getPrinciple()*cust.getTenure()*rateOfInterset)/100;
		
	}

	

	
}
