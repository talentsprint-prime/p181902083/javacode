package Interface;

public class Customer {
	private  int custid;
	private  String name;
	private  String acctNo;
	private  double principle;
	private  int tenure;
	
	
	public Customer() {
		super();
	}


	public Customer(int custid, String name, String acctNo, double principle, int tenure) {
		super();
		this.custid = custid;
		this.name = name;
		this.acctNo = acctNo;
		this.principle = principle;
		this.tenure = tenure;
	}


	public int getCustid() {
		return custid;
	}


	public void setCustid(int custid) {
		this.custid = custid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAcctNo() {
		return acctNo;
	}


	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}


	public double getPrinciple() {
		return principle;
	}


	public void setPrinciple(double principle) {
		this.principle = principle;
	}


	public int getTenure() {
		return tenure;
	}


	public void setTenure(int tenure) {
		this.tenure = tenure;
	}


	@Override
	public String toString() {
		return "Customer [custid=" + custid + ", name=" + name + ", acctNo=" + acctNo + ", principle=" + principle
				+ ", tenure=" + tenure + "]";
	}
	
}
	 