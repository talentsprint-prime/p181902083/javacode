package Interface;

import java.util.Scanner;

public class BillDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int choice = 0;
		int units = 0;
		while (true) {
			System.out.println("1.ElectricBill \n2.WaterBill \n3.Exit");

			System.out.println("Enter choice:");
			choice = sc.nextInt();
			if (choice == 1 || choice == 2) {
				System.out.println("Enter no.of.unts:");
				units = sc.nextInt();
			}

			switch (choice) {
			case 1:
				ElectricBill eb = new ElectricBill();
				eb.setNo_of_units(units);
				eb.displayBill();
				break;
			case 2:
				WaterBill wb = new WaterBill();
				wb.setNo_of_units(units);
				wb.displayBill();
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid choice");
			}

		}

	}
}