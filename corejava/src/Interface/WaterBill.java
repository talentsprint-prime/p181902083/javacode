package Interface;

public class WaterBill implements Bill {
	public static final int per_unit_charge = 5;
	private int no_of_units;
	
	public WaterBill() {
		super();
	}
	
	
	public WaterBill(int no_of_units) {
		super();
		this.no_of_units = no_of_units;
	}
	
	public int getNo_of_units() {
		return no_of_units;
	}

	public void setNo_of_units(int no_of_units) {
		this.no_of_units = no_of_units;
	}

	

	@Override
	public double calculateBill() {
		return per_unit_charge * no_of_units;
		
	}
	@Override
	public void displayBill() {
	System.out.println("==========WaterBill=========");
	System.out.println("No.of.units:" +no_of_units);
	System.out.println("No.of.units.per.charge: "+per_unit_charge);
	System.out.println("Total Amount: "+calculateBill());
		
	}

}
