
public class AverageWordLength {

	public static void main(String[] args) {
		System.out.println(AverageWordLength.getAverageWordLength("Hi mom"));
		System.out.println(AverageWordLength.getAverageWordLength("hi everyone"));
		System.out.println(AverageWordLength.getAverageWordLength("how are you"));

	}

	public static int getAverageWordLength(String str) {
		if(str == null)
			return -1;
		if(str == " ")
			return -2;
		
		String arr[] = str.split(" ");

		int No_of_words = 0;
		int No_of_chars = 0;

		for (int i = 0; i < arr.length; i++) {
			No_of_words++;
		}
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			No_of_chars++;
			if (c == ' ')
				No_of_chars--;
		}

		return No_of_chars / No_of_words;

	}

}
