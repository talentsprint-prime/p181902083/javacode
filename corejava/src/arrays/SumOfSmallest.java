package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class SumOfSmallest {

	public static void main(String[] args) {
		int[] arr = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter array elements:");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println(sum(arr));

	}

	public static int sum(int[] arr) {

		Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));

		return arr[0] + arr[1];
	}
}
