package arrays;

import java.util.Scanner;

public class NoOfOccurences {

	public static void main(String[] args) {
		int[] arr = new int[7];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter elements:");
		for(int i = 0;i < arr.length;i++)
		{
			arr[i] = sc.nextInt();
		}
		System.out.println(occurences(arr,5));
		}

	
	public static int occurences(int[] arr,int element){
		int count = 0;
		for(int i = 0;i < arr.length;i++){
			if(arr[i] == element){
				count++;
			}
		}
		return count;

}
}