package arrays;

import java.util.Scanner;

public class Count {

	public static void main(String[] args) {
		int[] arr = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter elements:");
		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
		System.out.println(countEvenAndOdd(arr));
	}

	public static String countEvenAndOdd(int[] arr) {
		int evenCount = 0;
		int oddCount = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % 2 == 0)
				evenCount++;
			else
				oddCount++;
		}
		return "EvenCount:" + evenCount + " OddCount:" + oddCount;
	}

}
