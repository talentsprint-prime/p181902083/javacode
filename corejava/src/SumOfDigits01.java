
public class SumOfDigits01 {

	public static void main(String[] args) {
		int num = 799;
		System.out.println(getSumOfDigits(num));
	}

	public static int getSumOfDigits(int num) {
		int sum = 0;
		if (num < 9 || num > 100)
			return 0;

		if (num > 9 && num < 100) {
			sum = (num % 10) + (num / 10);

		}
		return sum;
	}
}
