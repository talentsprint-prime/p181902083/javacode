
public class StarPyramid02 {
	public static void main(String[] args) {
		System.out.print(starPattern(5));
	}

	public static String starPattern(int rows) {
		String str = "";
		int count = rows - 1;
		if (rows < 0)
			return "-1";
		if (rows == 0)
			return "-2";

		for (int i = 1; i <= rows; i++) {
			for(int k = 1;k <= count;k++){
				str += " ";
			}
			count--;
			
			for (int j = 1; j <= 2 * i - 1; j++) {
				str += "*";
			}
			str += "\n";
		}

		return str;
	}

}
