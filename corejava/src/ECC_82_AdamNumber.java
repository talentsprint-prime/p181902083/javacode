
public class ECC_82_AdamNumber {
	public static void main(String[] args) {
        int num =20;
      
        System.out.println(isAdamNumber(num));
    }
    
   
    public static boolean isAdamNumber(int num) {
    	int a = getSquare(num);

    	int b = getReverse(num);
    	int c = getSquare(b);

    	int d = getReverse(c);

    	return (a == d);
    	 
    	
    }

   
    public static int getReverse(int n) {
    	int r = 0;
    	int rev =0;
      while(n > 0){
    	  r = n % 10;
    	  r = n % 10;
          rev = (rev * 10) + r;
          n = n/10;
      }
      return rev;
    }

   
    public static int getSquare(int n) {
        
    	return n * n;
    }
}
