
public class Collatz01 {
	public static void main(String[] args) {
		System.out.println(getCollatzSequence(5));
	}

	public static String getCollatzSequence(int number) {
		String str = "";
		int counter = 0;
		if (number <= 0)
			return "Error";

		while (number != 4) {
			counter++;
			str = str + number + " ";
			if (number % 2 != 0)
				number = ((3 * number) + 1);
			else
				number = number / 2;
			
			if(counter >= 97 && number != 8)
				return "Does not Converge";
		}
		str += "4 2 1";

		return str;
	}

}
