
public class RoundedSum {
	public static void main(String[] args) {
		int a = 20, b = 30, c = 40;
		System.out.println(sumOfRoundedValues(a, b, c));
	}

	public static int sumOfRoundedValues(int n1, int n2, int n3) {
		if (n1 <= 0 || n2 <= 0 || n3 <= 0)
			return -1;

		if (n1 % 10 != 0)
			if (n1 % 10 < 5)
				n1 = ((n1 / 10)) * 10;
			else
				n1 = ((n1 / 10) + 1) * 10;

		if (n2 % 10 != 0)
			if (n2 % 10 < 5)
				n2 = ((n2 / 10)) * 10;
			else
				n2 = ((n2 / 10) + 1) * 10;

		if (n3 % 10 != 0)
			if (n3 % 10 < 5)
				n3 = ((n3 / 10)) * 10;
			else
				n3 = ((n3 / 10) + 1) * 10;

		return n1 + n2 + n3;
	}
}
