package staticprogram;

public class Student1Main {

	public static void main(String[] args) {

		Student1 s1 = new Student1("Ajay", 60, 60, 60);
		System.out.println(s1.toString());

		Student1 s2 = new Student1("abc", 50, 70, 90);
		System.out.println(s2);
		
		Student1 s3 = new Student1("xyz", 90, 90, 90);
		System.out.println(s3);

		Student1 s = Student1.compare(s1,Student1.compare(s2,s3));
		System.out.println("Student with highest percentagae: "+s);

	}

}
