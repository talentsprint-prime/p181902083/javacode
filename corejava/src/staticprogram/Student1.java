package staticprogram;

public class Student1 {

	private int id;
	private String name;
	private int sub1, sub2, sub3;

	public static int idGenerator = 100;

	public Student1() {
		super();
		this.id = ++idGenerator;
	}

	public Student1(String name, int sub1, int sub2, int sub3) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.sub1 = sub1;
		this.sub2 = sub2;
		this.sub3 = sub3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSub1() {
		return sub1;
	}

	public void setSub1(int sub1) {
		this.sub1 = sub1;
	}

	public int getSub2() {
		return sub2;
	}

	public void setSub2(int sub2) {
		this.sub2 = sub2;
	}

	public int getSub3() {
		return sub3;
	}

	public void setSub3(int sub3) {
		this.sub3 = sub3;
	}

	public double calcPercentage() {
		return (sub1 + sub2 + sub3) / 3.0;
	}
	
	public static Student1 compare(Student1 s1,Student1 s2){
	
		if(s1.calcPercentage() > s2.calcPercentage())
			return s1;
		else 
			return s2;
	}

	@Override
	public String toString() {
		return "Student1 [id=" + id + ", name=" + name + ", Percentage=" + calcPercentage() + "]";
	}

}
