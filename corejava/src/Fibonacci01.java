
public class Fibonacci01 {
	public static void main(String[] args) {
		System.out.println(getFibonacciSeries(3));
	}

	public static String getFibonacciSeries(int num) {

		int f1 = 0, f2 = 1, f3;
		String str = "";
		String str1 = "";
		
		if(num < 1 || num > 40)
			return "-1";
		
		for (int i = 0; i < num; i++) {
			str = str + f1 + ",";
			f3 = f1 + f2;
			f1 = f2;
			f2 = f3;
			
			str1 = str.substring(0,str.length()-1);
			
		}
		return str1;
	}
}
