package mobile;

public class MobileMain {

	public static void main(String[] args) {

		Mobile samsung = new Mobile();
		
		samsung.RAM = 4;
		samsung.processor = 11.4f;
		samsung.battery = 35000;
		
		System.out.println("RAM: "+samsung.RAM);
		System.out.println("Processor: "+samsung.processor);
		System.out.println("Battery: "+samsung.battery);
		
		samsung.calling();
	}

}
