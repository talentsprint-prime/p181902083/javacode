package association;

public class Address {
	private String Hno;
	private String street;
	private String city;
	private String state;
	private double pincode;
	public Address(){
		super();
	}
	public Address(String hno, String street, String city, String state, double pincode) {
		super();
		Hno = hno;
		this.street = street;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
	}
	public String getHno() {
		return Hno;
	}
	public void setHno(String hno) {
		Hno = hno;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getPincode() {
		return pincode;
	}
	public void setPincode(double pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "Address [Hno=" + Hno + ", street=" + street + ", city=" + city + ", state=" + state + ", pincode="
				+ pincode + "]";
	}
	

}
