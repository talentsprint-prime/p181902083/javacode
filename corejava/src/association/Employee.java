package association;

public class Employee {
	private int  empid;
	private String empname;
	private Double salary;
	private String desgination;
	Address add;
	
	public Employee(){
		super();
		add = new Address();
	}
	
	/*public Employee(int empid, String empname, Double salary, String desgination) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.salary = salary;
		this.desgination = desgination;
	}*/
	
	public Employee(int empid, String empname, Double salary, String desgination, Address add) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.salary = salary;
		this.desgination = desgination;
		this.add = add;
	}

	


	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getDesgination() {
		return desgination;
	}

	public void setDesgination(String desgination) {
		this.desgination = desgination;
	}


	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}

	
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", empname=" + empname + ", salary=" + salary + ", desgination="
				+ desgination + ", add=" + add + "]";
	}

}
