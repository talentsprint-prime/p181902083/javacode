
public class JulianDate01 {
	
	public static void main(String[] args) {
        String date = "23-Jan-2016";
        System.out.println(dateFormat(date));
    }
   
    public static String dateFormat(String date) {
    	String[] dateparts = date.split("-");
	     
	    int dd = Integer.parseInt(dateparts[0]);
	    int mm = convertMMMtoMM(dateparts[1]);
	    int year = Integer.parseInt(dateparts[2]);
	    return year + julianDate(dd,mm);
    }
   
   public static String julianDate(int dd, int mon) {
	   int months[] = {0,31,59,90,120,151,181,212,243,273,304,334,365};
	   int j =months[mon - 1] + dd;
	   String day = "" + j;
	   if(j <= 9)
		   day = "00" + j;
	   else if(j <= 99)
		   day = "0" + j;
	   
	   
	   return day;
   }
      
   
   
   public static int convertMMMtoMM(String mon) {
	   String Months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();
		int m = ((Months.indexOf(mon) / 3) + 1);
		
		return m;
	
   }

}
