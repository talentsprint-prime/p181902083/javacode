
public class Palindrome01 {

	public static void main(String[] args) {
		int num = 878;
		System.out.println(isPalindrome(num));
	}

	public static int isPalindrome(int num) {
		int r = 0, rev = 0;
		int count = 0;
		if(num < 100 || num >= 999)
			return -1;

		else
			while (num > 0) {
				r = num % 10;
				rev = rev * 10 + r;
				num = num / 10;
			}
			if (num != rev)
				return 0;
			else
				return 1;
		}
	

}

