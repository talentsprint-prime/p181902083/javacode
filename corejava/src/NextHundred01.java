
public class NextHundred01 {

	public static void main(String[] args) {

		System.out.println(getNextMultipleOf100(223));

	}

	public static int getNextMultipleOf100(int num) {
		
		if (num < 0 || num == 0)
			return -1;
		else
			num = num + (100 - num % 100);

		return num;

	}

}
