
public class SpinningString {
	public static void  main(String[] arg) {
		System.out.println(SpinningString.rotate("", 1));
		System.out.println(SpinningString.rotate("talent", 0));
		System.out.println(SpinningString.rotate("talent", 3));
		
		
	}

	public static String rotate(String str, int no_of_positions) {
		if(no_of_positions <= 0)
			return str;
		if(str == null)
			return null;
		if( no_of_positions == 0)
			return null;
		if(no_of_positions > str.length())
			return str;
		
		
		char arr[] = str.toCharArray();
		String  str1 = "";
		for(int j = 0 ;j < no_of_positions;j++){
			char ch = arr[arr.length-1];
			for(int i = arr.length - 1;i > 0;i--){
				arr[i] = arr[i-1];
		}
			arr[0] = ch;
		}
		for(int i = 0;i<arr.length;i++)
			str1 += arr[i];
	
	return str1;
}
}
