
public class TwinPrimes01 {

	public static void main(String[] args) {
		int num1 = 2;
		int num2 = 50;
		System.out.println(isPrime(2));
		System.out.println(twinPrimes(num1, num2));
	}

	public static String twinPrimes(int start, int limit) {

		String str = "";
		String str1 ="";
		
		
		if (start <= 0 || limit <= 0)
			return "-1";
		
		if (start >= limit)
			return "-2";

		for (int i = start; i <= limit; i++) {
			if (isPrime(i) && isPrime(i + 2)) {
				str = str + i + ":" + (i + 2) + ",";
				
			}
		}
		if (str.length() == 0)
			return "-3";
		else
			str1 = str.substring(0, str.length()-1);
		    return str1;
	}

	public static boolean isPrime(int num) {
		if(num == 1)
			return false;
		/*if(num == 2)
			return true;*/

		for (int i = 2; i <= num / 2; i++) {
			if (num % i == 0)
				return false;
		}
		return true;
	}

}
