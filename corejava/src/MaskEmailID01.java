
public class MaskEmailID01 {
	public static void main(String[] args) {
		
		String email = "tesmail@.mailme.com";
		System.out.println(maskMailID(email));
	
	}

	public static String maskMailID(String email) {
		int index = email.indexOf("@");
		
		String end = email.substring(index);
		String mask = "";
		
		int len = email.substring(2, index).length();
		
		for (int i = 2; i <= len+1; i++) {
			mask += "X";
		}
		mask = email.substring(0, 2) + mask + end;
		return mask;
	}
}