
public class ArmStrong01 {

	public static void main(String[] args) {
		int num1 = 150;
		int num2 = 300;
		/*System.out.println(power(2, 3));
		System.out.println(sumOfPowersOfDigits(153));
		
		System.out.println(isArmstrong(153)); 
		System.out.println(generateArmstrongNums(num1, num2));*/
		System.out.println(getDigits(153));
	}

	public static String generateArmstrongNums(int start, int limit) {
		String result = "";

		if (start <= 0 || limit <= 0)
			return "-1";

		if (start >= limit)
			return "-2";

		for (int i = start; i <= limit; i++) {

			if (isArmstrong(i))
				result = result + i + ",";

		}
		if (result.length() == 0)
			return "-3";
		else
			result = result.substring(0, result.length() - 1);
		return result;
	}

	public static boolean isArmstrong(int num) {
		return (num == sumOfPowersOfDigits(num));
	}

	public static int sumOfPowersOfDigits(int n) {
		int digits[] = getDigits(n);
		int sum = 0;
		for(int i = 0; i < digits.length;i++)
			sum += power(digits[i],digits.length);
		return sum;
	}

	public static int[] getDigits(int n) {
		String s = n + " ";
		int digits[] = new int[s.length()];
		int i = 0;
		
		while (n > 0) {
			digits[i++] = n % 10;
			n /= 10;
		}
		
			
			
		return digits;
	}

	public static int power(int d, int p) {
		return (int)Math.pow(d, p);
	}
}
