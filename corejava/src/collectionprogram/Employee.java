package collectionprogram;

public class Employee implements Comparable<Employee>{
	private int id;
	private String name;
	private double salary;

	public static int idGenerator = 100;

	public Employee() {
		super();
		this.id = ++idGenerator;
	}

	public Employee(String name, double salary) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public static int getIdGenerator() {
		return idGenerator;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
/*
	@Override
	public int compareTo(Employee o) {
		if(this.getSalary() > o.getSalary()){
			return 1;
		}else if(this.getSalary() < o.getSalary()){
			return -1;
		}else
			return 0;
	}*/
	@Override
	public int compareTo(Employee o) {
		
		/*String s1 = this.getName();
		String s2 = o.getName();
		System.out.println(s1);
		System.out.println(s2);
		*/
		return this.getName().compareTo(o.getName());
		
			
	}
	

}
