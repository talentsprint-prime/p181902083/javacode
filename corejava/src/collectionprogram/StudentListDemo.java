package collectionprogram;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StudentListDemo {

	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<>();

		Student s1 = new Student("laxmi", 60, 60, 60);
		studentList.add(s1);
		studentList.add(new Student("abc", 75, 85, 65));
		//studentList.add(new Student("pqr", 55, 65, 60));

		for (Student s : studentList) {
			System.out.println("Id:" + s.getId() + " name:" + s.getName() + " percentage:" + s.percentage());

		}
	
	
	Iterator<Student> itr = studentList.iterator();
	while(itr.hasNext()){
		Student s2 = itr.next();
		//System.out.println("Id:" + s.getId() + " name:" + s.getName() + " percentage:" + s.percentage());
		System.out.println(itr.next());
	}

}
}
