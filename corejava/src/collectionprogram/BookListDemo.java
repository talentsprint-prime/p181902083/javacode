package collectionprogram;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BookListDemo {

	public static void main(String[] args) {

		List<Book> b = new ArrayList<>();

		b.add(new Book("Two States", "ChethanBhagath", "A"));
		b.add(new Book("abc", "Bhagath", "A"));
		b.add(new Book("pqr", "lmn", "B"));
		b.add(new Book("abc", "cgd", "C"));

		Iterator<Book> itr = b.iterator();
		for (Book b1 : b) {
			if (b1.getPublisher().equals("A"))

				System.out.println("Name:" + b1.getName() + " Publisher:" + b1.getPublisher());
		}

	}

}
