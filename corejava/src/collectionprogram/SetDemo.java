package collectionprogram;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
	
		Set<String> colourset = new HashSet<>();
		
		colourset.add("Red");
		colourset.add("Yellow");
		colourset.add("Blue");
		colourset.add("Orange");
		colourset.add("Red");
		
		System.out.println(colourset);
		

		Set<String> nameset = new LinkedHashSet<>();
		
		nameset.add("laxmi");
		nameset.add("supriya");
		nameset.add("keerthi");
		nameset.add("shivani");
		nameset.add("alekhya");
		nameset.add("laxmi");
		
		System.out.println(nameset);
		
		
		Set<Integer> numberSet = new TreeSet<>();
		
		numberSet.add(1);
		numberSet.add(25);
		numberSet.add(10);
		numberSet.add(71);
		numberSet.add(1);
		
		System.out.println(numberSet);
	}

}
