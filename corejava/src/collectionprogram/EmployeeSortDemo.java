package collectionprogram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeSortDemo {

	public static void main(String[] args) {
		
		List<Employee> emplist = new ArrayList<>();
		
		emplist.add(new Employee("laxmi",15000d));
		emplist.add(new Employee("abc",25000d));
		emplist.add(new Employee("lax",20000d));
		emplist.add(new Employee("xyz",17000d));
		emplist.add(new Employee("abcd",35000d));
		
		System.out.println("==========BeforeSort=========");
		for(Employee e : emplist){
			System.out.println(e);
		}
		
		Collections.sort(emplist);
		
		System.out.println("==========AfterSort==========");
		for(Employee e : emplist){
			System.out.println(e);
		}

	}

}
