package collectionprogram;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {

		List<Integer> yearList = new ArrayList<>();

		yearList.add(1996);
		yearList.add(1998);
		yearList.add(2000);
		yearList.add(2010);
		yearList.add(2019);

		yearList.remove(1);
		yearList.remove(new Integer(2000));
		
		System.out.println(yearList.size());
		System.out.println(yearList.get(0));

		for (Integer list : yearList) {
			System.out.println(list);
		}

	}

}
