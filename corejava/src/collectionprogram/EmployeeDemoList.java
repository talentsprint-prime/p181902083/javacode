package collectionprogram;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmployeeDemoList {

	public static void main(String[] args) {
	
		List<Employee> e = new ArrayList<>();
		
		e.add(new Employee("laxmi",15000d));
		e.add(new Employee("abc",25000d));
		e.add(new Employee("lax",20000d));
		e.add(new Employee("xyz",17000d));
		e.add(new Employee("abcd",35000d));
		
		List<Employee> fresherList = new ArrayList<>();
		
		
		/*for(Employee e1 : e){
			if(e1.getSalary() < 20000){
				e.remove(e1);
				fresherList.add(e1);
			}
		}
		for(Employee e2 : e){
			System.out.println(e2);
		}
		for(Employee f: fresherList){
			System.out.println(f);
		}*/
		
		Iterator<Employee> itr = e.iterator();
		
		while(itr.hasNext()){
			Employee e1 = itr.next();
			if(e1.getSalary() < 20000 ){
				itr.remove();
				fresherList.add(e1);
			}
		}
		System.out.println("=========List=========");
		for(Employee e2 : e){
			System.out.println(e2);
		}
		System.out.println("==========FreshersList=========");
		for(Employee f: fresherList){
			System.out.println(f);
		}
	}

}
